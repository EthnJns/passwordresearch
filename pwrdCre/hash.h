#include<iostream>
#include<vector>
#include<string>
#include<cstdlib>
#include<time.h>

using namespace std;

#ifndef HASH_H
#define HASH_H

class Node{
  public:
    string data;
    Node* next;
};

class HashTable{
private:
    //int key;
    Node* ht[1000];

public:

    HashTable();
    ~HashTable();
    Node* createNode(string pwd);
    void push(string pwd);
    int hash(string pwd);
    bool found(string pwd);
    //void setKey(int key0){key = key0;};
    //int getKey(){return key};
};

#endif