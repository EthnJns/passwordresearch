#include "hash.h"

HashTable::HashTable(){
  //Initializing Hash Table to NULL Values
  for (int i = 0; i < 1000; i++){
    Node* gnuNode = new Node();
    gnuNode -> data = "";
    gnuNode -> next = NULL;
    ht[i] = gnuNode;
  }
}

//Use Protection
HashTable::~HashTable(){
  for(int i = 0; i < 1000; i++){
    if(ht[i] != NULL){
      Node* start = ht[i];
      while(start != NULL){
        Node* delNode = start;
        start = start -> next;
        delNode -> data = "";
        delNode -> next = NULL;
        delete(delNode);
      }
    }
  }
}

int HashTable::hash(string pwd){
  srand(time(0));
  int key = 0;
  int v1 = 7,v2 = 11,v3 = 13;
  for(int i = 0; i < pwd.length(); i++){
    v1 += i*(pwd[0]+(rand()%pwd.length()));
    v2 *= v1+pwd[0]+(pwd[3]+(rand()%pwd.length()));
    v3 += v2 - rand()%pwd.length();
     //printf("v1:\t%d\nv2:\t%d\nv3:\t%d\n",v1,v2,v3);
  }
  //cout<< ((v1*v2)+v3)%1000 <<endl;
  //cout<<"-----------------------------\n\n";
  key = ((v1*v2)+v3)%1000;

  if(key < 0){
    key*=-1;
  }
  return key;
}

Node* HashTable::createNode(string pwd){
    Node* gnuNode = new Node();
    gnuNode -> data = pwd;
    gnuNode -> next = NULL;

    return gnuNode;
}

void HashTable::push(string pwd){
  int key = hash(pwd);
  if(ht[key]->data == ""){
      ht[key] = createNode(pwd);
  }
  else{
    Node* iterator = ht[key];
    while(iterator -> next != NULL){
      iterator = iterator -> next;
    }
    iterator -> next = createNode(pwd);
  }
}

bool HashTable::found(string pwd){
  int key = hash(pwd);
  if(ht[key] -> data == pwd)
    return true;
  else{
    Node* start = ht[key];
    while(start != NULL){
      if(start->data == pwd)
        return true;
      else
        start = start -> next;
    }
    return false;
  }
}


/*
int main(){
  HashTable htable;

  htable.push("password");
  htable.push("password1");
  htable.push("pwdrddd23");

  if(htable.found("password")){
    cout<<"Key: "<<htable.hash("password")<<endl;
    cout<<"Found\n";
  }
  else
    cout<<"Something is wrong\n";
  if(htable.found("password1")){
    cout<<"Key: "<<htable.hash("password1")<<endl;
    cout<<"Found\n";
  }
  else
    cout<<"Something is wrong\n";
  if(htable.found("pwdrddd23")){
      cout<<"Key: "<<htable.hash("pwdrddd23")<<endl;
      cout<<"Found\n";
  }
  else
      cout<<"Something is wrong\n";
  if(!htable.found("dontwork"))
    cout<<"Working\n";
  else{
    cout<<"Not Working";
  }


  return 0;
}
*/
