# Need to calculate distances what works best
# Using a dictionary of dictionaries for quick look up
# Finally using a function
import math
def get_distance(s, e, keyB,shift_dict):
    distance = -1
    s = unShift(s, shift_dict)
    e = unShift(e, shift_dict)
    sRow,eRow = (-1, -1)
    sCol,eCol = (-1, -1)
    found_s, found_e = (False,False)
    while (found_s == False or found_e == False):
        for x in range(14):
            if keyB[0][x] == s:
                sRow = 0
                sCol = x
                found_s = True
            if keyB[1][x] == s:
                sRow = 1
                sCol = x
                found_s = True
            if keyB[2][x] == s:
                sRow = 2
                sCol = x
                found_s = True
            if keyB[3][x] == s:
                sRow = 3
                sCol = x
                found_s = True
            if keyB[0][x] == e:
                eRow = 0
                eCol = x
                found_e = True
            if keyB[1][x] == e:
                eRow = 1
                eCol = x
                found_e = True
            if keyB[2][x] == e:
                eRow = 2
                eCol = x
                found_e = True
            if keyB[3][x] == e:
                eRow = 3
                eCol = x
                found_e = True

    distR = abs(eRow - sRow)
    distC = abs(eCol - sCol) - distR
    if(distC < 0):
        distC = 0
    distance = distR + distC


    return distance

def unShift(ch, shift_dict):
    if ch == '|':
        return '\\'
    if ch.isalpha():
        return ch.lower()
    elif ch.isdigit():
        return ch
    else:
        if ch not in shift_dict.keys():
            return ch
        else:
            return shift_dict[ch]


#initialization of keyboard to find distance and dictionary to unshift values
keyB = [['`','1','2','3','4','5','6','7','8','9','0','-','=','\n'], ['\n','q','w','e','r','t','y','u','i','o','p','[',']','\\'],['\n','a','s','d','f','g','h','j','k','l',';','\'','\n','\n'], ['\n','z','x','c','v','b','n','m',',','.','/','\n','\n','\n']]
shift_list =  ['~','!','@','#','$','%','^','&','*','(',')','_','+','{','}',':','\"','<','>','?']
unshift_list= ['`','1','3','4','5','6','7','8','9','0','-','=','[',']','\\',';','\'',',','.','/']
shift_dict = {}
print(len(shift_list))
print(len(unshift_list))
i = 0
for s in shift_list:
    shift_dict[s] = unshift_list[i]
    i = i +1




popDict = "`1234567890-=qwertyuiop[]\\asdfghjkl;'zxcvbnm,./"
distDict = {}

print("Populating Dictionary")
for ch in popDict:
    if ch not in distDict.keys():
        distDict[ch] = {}
        for ch0 in popDict:
            if ch0 not in distDict[ch].keys():
                distDict[ch][ch0] = get_distance(ch, ch0, keyB, shift_dict)


#Test distance function -- WORKING
'''
print("a - 5: ",get_distance('a','5',keyB,shift_dict))
print("y - f: ",get_distance('y','f',keyB,shift_dict))
print("q - s: ",get_distance('q','s',keyB,shift_dict))
print("q - a: ",get_distance('q','a',keyB,shift_dict))
print("g - u: ",get_distance('g','u',keyB,shift_dict))
print("n - 0: ",get_distance('n','0',keyB,shift_dict))
print("c - =: ",get_distance('c','=',keyB,shift_dict))
print("z - \\: ",get_distance('z','\\',keyB,shift_dict))
'''


distPerc = {}
passdata = open('ry.txt', 'r', encoding = 'latin1')
passwords = passdata.readlines()
avg_dist = 0

print("Starting Password Distance Stuff: ")

for pwd in passwords:
    for ch in range(int(len(pwd))-2):
        try:
            if pwd[ch] == ' ' or pwd[ch+1] == ' ':
                continue
            if pwd[ch] == '\n' or pwd[ch+1] == '\n':
                continue
            s = unShift(pwd[ch],shift_dict)
            e = unShift(pwd[ch+1],shift_dict)
            dist = distDict[s][e]
            if dist not in distPerc.keys():
                distPerc[dist] = 1
            else:
                distPerc[dist] = distPerc[dist] + 1
            avg_dist = avg_dist + dist
        except:
            continue
total_perc = 0
for x in distPerc:
    print("Total occurences of distance ",x,": ", distPerc[x])
    total_perc = total_perc + distPerc[x]

for x in distPerc:
    print("Total percentage use of distance ",x,": ", distPerc[x]/total_perc)
