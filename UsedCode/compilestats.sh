python analysis.py > /root/Documents/prog/python/projects/pwrd_ergo/Results/phpbb_analysis.txt
python phase1.py > /root/Documents/prog/python/projects/pwrd_ergo/Results/phpbb_phase1.txt
python phase3.py > /root/Documents/prog/python/projects/pwrd_ergo/Results/phpbb_phase3.txt
python phase3_2.py > /root/Documents/prog/python/projects/pwrd_ergo/Results/phpbb_phase3_2.txt
python phase4.py > /root/Documents/prog/python/projects/pwrd_ergo/Results/phpbb_phase4.txt
python phase4_2.py > /root/Documents/prog/python/projects/pwrd_ergo/Results/phpbb_phase4_2.txt
python phase5.py > /root/Documents/prog/python/projects/pwrd_ergo/Results/phpbb_phase5.txt
python length.py > /root/Documents/prog/python/projects/pwrd_ergo/Results/phpbb_length.txt

cd /root/Documents/prog/python/projects/pwrd_ergo/Results/

touch phpbbResults.txt
cat phpbb_analysis.txt >> phpbbResults.txt
echo "------------------------------" >> phpbbResults.txt
cat phpbb_phase1.txt >> phpbbResults.txt
echo "------------------------------" >> phpbbResults.txt
cat phpbb_phase3.txt >> phpbbResults.txt
echo "------------------------------" >> phpbbResults.txt
cat phpbb_phase3_2.txt >> phpbbResults.txt
echo "------------------------------" >> phpbbResults.txt
cat phpbb_phase4.txt >> phpbbResults.txt
echo "------------------------------" >> phpbbResults.txt
cat phpbb_phase4_2.txt >> phpbbResults.txt
echo "------------------------------" >> phpbbResults.txt
cat phpbb_phase5.txt >> phpbbResults.txt
echo "------------------------------" >> phpbbResults.txt
cat phpbb_length.txt >> phpbbResults.txt
echo "------------------------------" >> phpbbResults.txt
