#The following program will find
#Where in passwords the quadrants were most likely to occur

patterns = open('allPatt4.txt', 'r')
pattList = patterns.readlines()

#Time to collect all the data
#Dictionaries for 4 quadrants
in_first_section = {'1': 0, '2':0, '3':0, '4':0,'5': 0, '6':0, '7':0, '8':0}

in_scond_section = {'1': 0, '2':0, '3':0, '4':0,'5': 0, '6':0, '7':0, '8':0}

in_third_section = {'1': 0, '2':0, '3':0, '4':0,'5': 0, '6':0, '7':0, '8':0}

in_fourt_section = {'1': 0, '2':0, '3':0, '4':0,'5': 0, '6':0, '7':0, '8':0}


#Lists of sections
section_1 = []
section_2 = []
section_3 = []
section_4 = []

#The number of sections will be how far after each character we take the substring
#Populate the lists of sections 1 - 4 to check for instances
for pattern in pattList:
    size_of_pattern = int(len(pattern))
    if(size_of_pattern < 4):
        continue
    number_of_sections = int(size_of_pattern / 4)
    if(size_of_pattern % 4 >= 2 ):
        if size_of_pattern == 6:
            number_of_sections = 1
        else:
            number_of_sections = number_of_sections = 1
    section_1.append(pattern[0: number_of_sections])
    section_2.append(pattern[number_of_sections: number_of_sections + number_of_sections])
    section_3.append(pattern[number_of_sections + number_of_sections: number_of_sections + number_of_sections + number_of_sections])
    section_4.append(pattern[number_of_sections + number_of_sections + number_of_sections:])


for part in section_1:
    for character in part:
        in_first_section[character] =  in_first_section[character] + 1

for part in section_2:
    for character in part:
        in_scond_section[character] =  in_scond_section[character] + 1

for part in section_3:
    for character in part:
        in_third_section[character] =  in_third_section[character] + 1

for part in section_4:
    for character in part:
        if character == '\n':
            continue
        in_fourt_section[character] =  in_fourt_section[character] + 1

total1 = in_first_section['1'] + in_scond_section['1'] + in_third_section['1'] + in_fourt_section['1']
total2 = in_first_section['2'] + in_scond_section['2'] + in_third_section['2'] + in_fourt_section['2'] 
total3 = in_first_section['3'] + in_scond_section['3'] + in_third_section['3'] + in_fourt_section['3'] 
total4 = in_first_section['4'] + in_scond_section['4'] + in_third_section['4'] + in_fourt_section['4']

print("Total amount of quadrant 1 in part 1: ", in_first_section['1'])
print("Total amount of quadrant 2 in part 1: ", in_first_section['2'])
print("Total amount of quadrant 3 in part 1: ", in_first_section['3'])
print("Total amount of quadrant 4 in part 1: ", in_first_section['4'])
print("Total amount of quadrant 1 in part 2: ", in_scond_section['1'])
print("Total amount of quadrant 2 in part 2: ", in_scond_section['2'])
print("Total amount of quadrant 3 in part 2: ", in_scond_section['3'])
print("Total amount of quadrant 4 in part 2: ", in_scond_section['4'])
print("Total amount of quadrant 1 in part 3: ", in_third_section['1'])
print("Total amount of quadrant 2 in part 3: ", in_third_section['2'])
print("Total amount of quadrant 3 in part 3: ", in_third_section['3'])
print("Total amount of quadrant 4 in part 3: ", in_third_section['4'])
print("Total amount of quadrant 1 in part 4: ", in_fourt_section['1'])
print("Total amount of quadrant 2 in part 4: ", in_fourt_section['2'])
print("Total amount of quadrant 3 in part 4: ", in_fourt_section['3'])
print("Total amount of quadrant 4 in part 4: ", in_fourt_section['4'])


print("Total % amount of quadrant 1 in part 1: ", in_first_section['1']/total1)
print("Total % amount of quadrant 2 in part 1: ", in_first_section['2']/total1)
print("Total % amount of quadrant 3 in part 1: ", in_first_section['3']/total1)
print("Total % amount of quadrant 4 in part 1: ", in_first_section['4']/total1)
print("Total % amount of quadrant 1 in part 2: ", in_scond_section['1']/total2)
print("Total % amount of quadrant 2 in part 2: ", in_scond_section['2']/total2)
print("Total % amount of quadrant 3 in part 2: ", in_scond_section['3']/total2)
print("Total % amount of quadrant 4 in part 2: ", in_scond_section['4']/total2)
print("Total % amount of quadrant 1 in part 3: ", in_third_section['1']/total3)
print("Total % amount of quadrant 2 in part 3: ", in_third_section['2']/total3)
print("Total % amount of quadrant 3 in part 3: ", in_third_section['3']/total3)
print("Total % amount of quadrant 4 in part 3: ", in_third_section['4']/total3)
print("Total % amount of quadrant 1 in part 4: ", in_fourt_section['1']/total4)
print("Total % amount of quadrant 2 in part 4: ", in_fourt_section['2']/total4)
print("Total % amount of quadrant 3 in part 4: ", in_fourt_section['3']/total4)
print("Total % amount of quadrant 4 in part 4: ", in_fourt_section['4']/total4)
