#Tried running analysis on just the quadrant data
#Nothing really came about.
#

more_of_q1 = 5298293
more_of_q2 = 2932238
diff_1_2 = 0
diff_2_1 = 0

quad_data = open("quad_data.txt","r")
qd = quad_data.readlines()
#qd = ["12 | 15", "178 | 8"]
q1 = []
q2 = []

for x in qd:
    q1end = 0
    for v in x:
        q1end = q1end + 1
        if v == ' ':
            q1.append(int(x[0:q1end]))
            break
for x in qd:
    x = x[::-1]
    q1end = 0
    for v in x:
        q1end = q1end + 1
        if v == ' ':
            j = x[0:q1end]
            j = j[::-1]
            q2.append(int(j))
            break

for x in range(len(q1)):
    if q1[x] > q2[x]:
        diff_1_2 = diff_1_2 + (q1[x] - q2[x])
    if q2[x] > q1[x]:
        diff_2_1 = diff_2_1 + (q2[x] - q1[x])


avg_diff_1 = int(diff_1_2 / more_of_q1)
avg_diff_2 = int(diff_2_1 / more_of_q2)

print("Average Difference of Q1 and Q2 of Q1 dominated passwords: ", avg_diff_1, " more characters on avg")
print("Average Difference of Q2 and Q1 of Q2 dominated passwords: ", avg_diff_2, " more characters on avg")
