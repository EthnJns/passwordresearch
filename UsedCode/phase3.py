### Ethan Jones
### October 28th, 2019
### The following program is used to analyze passwords and  locate if specific trends exist
### Between key placement on a keyboard, and selection of passwords
# First we will get passwords, and find the average percentage of quadrants 1 & 2 on each password ex: on average passwords contained 40% use of Q1 and 60% use of Q2
# Then we will find most percentage use of Q1 and Q2 in sections of passwords, buy putting them in half, then thirds, then 4ths
# We will then look for percentage of specific letterings and qudrants coming after specific key strokes
# Goal in mind is to find any correlation between ergonomics and passwords, then use supervised learning to creat a bot that will look at some of your passwords
# Then it will try to guess a password you chose.

#Dictionary used for placing key strokes in 2 quadrants then move to 3 quadrants to n quadrants with evolution of project
##Progress

Quadrants = {}

#Create the dictionary for the quadrants
#I realized hardcoding the dictionary was tedious,
#And could be done in a much better fashion

pop_dic = ["`123qweasdzxc~!@#QWEASDZXC", "456rtyfghvbn$%^RTYFGHVBN", "789uiojklm,.&*(UIOJKLM<>", "0-=p[]\\;\'/)_+P{}|:\"?"]
for x in range(len(pop_dic)):
    for ch in pop_dic[x]:
        if ch not in Quadrants.keys():
            Quadrants[ch] = x+1
Quadrants [' '] = '2'
Quadrants['\n'] = -1000
#Initialize Dictionaries to obatin data and run calculations
dictFirst_End = {'1': [0,0,0,0],'2': [0,0,0,0],'3': [0,0,0,0],'4': [0,0,0,0]}
dictNext_Quad = {'1': [0,0,0,0],'2': [0,0,0,0],'3': [0,0,0,0],'4': [0,0,0,0]}
dicStart_Quad = {'1':0, '2':0, '3':0, '4':0}
dicEndin_Quad = {'1':0, '2':0, '3':0, '4':0}
dicTotal_Usag = {'1':0, '2':0, '3':0, '4':0}
avg1 = 0
avg2 = 0
avg3 = 0
avg4 = 0

#open passwords.txt to get ready to convert passwords to 1-4 patterns
#Uncomment password, passlist and change patterns to write
patterns = open('allPatt4.txt', 'r')
#passwords = open("ry.txt", "r", encoding="latin1")
#passList = passwords.readlines()
pattList = patterns.readlines()
'''
for pwrd in passList:
    pattern = ""
    for lett in pwrd:
        try:
            if(lett != '\n'):
                pattern = pattern + str(Quadrants[lett])
        except:
            continue
    pattList.append(pattern)
    patterns.write(pattern+"\n")
'''

#Time to collect all the data
#I will need variables to store values
#Since working with dictionaries, found a better way to collect the data I was looking for
#By first converting all passwords to the patterns, and collecting different types of info in a better way
for pattern in pattList:
    start = pattern[0]
    end = pattern[int(len(pattern)) -2]
    if end == '\n':
        continue
    dictFirst_End[start][int(end)-1] = dictFirst_End[start][int(end)-1] + 1
    dicStart_Quad[start] = dicStart_Quad[start] + 1
    dicEndin_Quad[end] = dicEndin_Quad[end]+1
    for num in range(int(len(pattern))-2):
        curr = pattern[num]
        next = pattern[num+1]
        inext = int(next)
        dictNext_Quad[curr][inext-1] = dictNext_Quad[curr][inext-1] + 1
        dicTotal_Usag[curr] = dicTotal_Usag[curr] + 1

#Get Totals for each
total = int(len(pattList))
totalNext = [0,0,0,0]
totalFnEn = [0,0,0,0]
totalStrt = dicStart_Quad['1'] + dicStart_Quad['2'] + dicStart_Quad['3'] + dicStart_Quad['4']
totalEndg = dicEndin_Quad['1'] + dicEndin_Quad['2'] + dicEndin_Quad['3'] + dicEndin_Quad['4']
totalLett = dicTotal_Usag['1'] + dicTotal_Usag['2'] + dicTotal_Usag['3'] + dicTotal_Usag['4']

totalNext[0] = dictNext_Quad['1'][0] + dictNext_Quad['1'][1] + dictNext_Quad['1'][2] + dictNext_Quad['1'][3]
totalNext[1] = dictNext_Quad['2'][0] + dictNext_Quad['2'][1] + dictNext_Quad['2'][2] + dictNext_Quad['2'][3]
totalNext[2] = dictNext_Quad['3'][0] + dictNext_Quad['3'][1] + dictNext_Quad['3'][2] + dictNext_Quad['3'][3]
totalNext[3] = dictNext_Quad['4'][0] + dictNext_Quad['4'][1] + dictNext_Quad['4'][2] + dictNext_Quad['4'][3]

totalFnEn[0] = dictFirst_End['1'][0] + dictFirst_End['1'][1] + dictFirst_End['1'][2] + dictFirst_End['1'][3]
totalFnEn[1] = dictFirst_End['2'][0] + dictFirst_End['2'][1] + dictFirst_End['2'][2] + dictFirst_End['2'][3]
totalFnEn[2] = dictFirst_End['3'][0] + dictFirst_End['3'][1] + dictFirst_End['3'][2] + dictFirst_End['3'][3]
totalFnEn[3] = dictFirst_End['4'][0] + dictFirst_End['4'][1] + dictFirst_End['4'][2] + dictFirst_End['4'][3]

#Perform Calculations
#Total use
print("Total use of quadrant 1: ", dicTotal_Usag['1'])
print("Total use of quadrant 2: ", dicTotal_Usag['2'])
print("Total use of quadrant 3: ", dicTotal_Usag['3'])
print("Total use of quadrant 4: ", dicTotal_Usag['4'])
print("Total percent use of quadrant 1: ", dicTotal_Usag['1']/totalLett)
print("Total percent use of quadrant 2: ", dicTotal_Usag['2']/totalLett)
print("Total percent use of quadrant 3: ", dicTotal_Usag['3']/totalLett)
print("Total percent use of quadrant 4: ", dicTotal_Usag['4']/totalLett)

#Total Start
print("Total Starting of Quadrant 1: ", dicStart_Quad['1'])
print("Total Starting of Quadrant 2: ", dicStart_Quad['2'])
print("Total Starting of Quadrant 3: ", dicStart_Quad['3'])
print("Total Starting of Quadrant 4: ", dicStart_Quad['4'])
print("Total Percent of Starting of Quadrant 1: ", dicStart_Quad['1']/totalStrt)
print("Total Percent of Starting of Quadrant 2: ", dicStart_Quad['2']/totalStrt)
print("Total Percent of Starting of Quadrant 3: ", dicStart_Quad['3']/totalStrt)
print("Total Percent of Starting of Quadrant 4: ", dicStart_Quad['4']/totalStrt)

#Total End
print("Total Ending of Quadrant 1: ", dicEndin_Quad['1'])
print("Total Ending of Quadrant 2: ", dicEndin_Quad['2'])
print("Total Ending of Quadrant 3: ", dicEndin_Quad['3'])
print("Total Ending of Quadrant 4: ", dicEndin_Quad['4'])
print("Total Percent of Ending of Quadrant 1: ", dicEndin_Quad['1']/totalEndg)
print("Total Percent of Ending of Quadrant 2: ", dicEndin_Quad['2']/totalEndg)
print("Total Percent of Ending of Quadrant 3:", dicEndin_Quad['3']/totalEndg)
print("Total Percent of Ending of Quadrant 4: ", dicEndin_Quad['4']/totalEndg)

#Total Next
print("Total of Quadrant 1 to 1: ",dictNext_Quad['1'][0])
print("Total of Quadrant 1 to 2: ",dictNext_Quad['1'][1])
print("Total of Quadrant 1 to 3: ",dictNext_Quad['1'][2])
print("Total of Quadrant 1 to 4: ",dictNext_Quad['1'][3])
print("Total of Quadrant 2 to 1: ",dictNext_Quad['2'][0])
print("Total of Quadrant 2 to 2: ",dictNext_Quad['2'][1])
print("Total of Quadrant 2 to 3: ",dictNext_Quad['2'][2])
print("Total of Quadrant 2 to 4: ",dictNext_Quad['2'][3])
print("Total of Quadrant 3 to 1: ",dictNext_Quad['3'][0])
print("Total of Quadrant 3 to 2: ",dictNext_Quad['3'][1])
print("Total of Quadrant 3 to 3: ",dictNext_Quad['3'][2])
print("Total of Quadrant 3 to 4: ",dictNext_Quad['3'][3])
print("Total of Quadrant 4 to 1: ",dictNext_Quad['4'][0])
print("Total of Quadrant 4 to 2: ",dictNext_Quad['4'][1])
print("Total of Quadrant 4 to 3: ",dictNext_Quad['4'][2])
print("Total of Quadrant 4 to 4: ",dictNext_Quad['4'][3])
print("Total Percent of Quadrant 1 to 1: ",dictNext_Quad['1'][0]/totalNext[0])
print("Total Percent of Quadrant 1 to 2: ",dictNext_Quad['1'][1]/totalNext[0])
print("Total Percent of Quadrant 1 to 3: ",dictNext_Quad['1'][2]/totalNext[0])
print("Total Percent of Quadrant 1 to 4: ",dictNext_Quad['1'][3]/totalNext[0])
print("Total Percent of Quadrant 2 to 1: ",dictNext_Quad['2'][0]/totalNext[1])
print("Total Percent of Quadrant 2 to 2: ",dictNext_Quad['2'][1]/totalNext[1])
print("Total Percent of Quadrant 2 to 3: ",dictNext_Quad['2'][2]/totalNext[1])
print("Total Percent of Quadrant 2 to 4: ",dictNext_Quad['2'][3]/totalNext[1])
print("Total Percent of Quadrant 3 to 1: ",dictNext_Quad['3'][0]/totalNext[2])
print("Total Percent of Quadrant 3 to 2: ",dictNext_Quad['3'][1]/totalNext[2])
print("Total Percent of Quadrant 3 to 3: ",dictNext_Quad['3'][2]/totalNext[2])
print("Total Percent of Quadrant 3 to 4: ",dictNext_Quad['3'][3]/totalNext[2])
print("Total Percent of Quadrant 4 to 1: ",dictNext_Quad['4'][0]/totalNext[3])
print("Total Percent of Quadrant 4 to 2: ",dictNext_Quad['4'][1]/totalNext[3])
print("Total Percent of Quadrant 4 to 3: ",dictNext_Quad['4'][2]/totalNext[3])
print("Total Percent of Quadrant 4 to 4: ",dictNext_Quad['4'][3]/totalNext[3])

#Total First and Ending
print("Total Start_End Quadrant 1 to 1: ",dictFirst_End['1'][0])
print("Total Start_End Quadrant 1 to 2: ",dictFirst_End['1'][1])
print("Total Start_End Quadrant 1 to 3: ",dictFirst_End['1'][2])
print("Total Start_End Quadrant 1 to 4: ",dictFirst_End['1'][3])
print("Total Start_End Quadrant 2 to 1: ",dictFirst_End['2'][0])
print("Total Start_End Quadrant 2 to 2: ",dictFirst_End['2'][1])
print("Total Start_End Quadrant 2 to 3: ",dictFirst_End['2'][2])
print("Total Start_End Quadrant 2 to 4: ",dictFirst_End['2'][3])
print("Total Start_End Quadrant 3 to 1: ",dictFirst_End['3'][0])
print("Total Start_End Quadrant 3 to 2: ",dictFirst_End['3'][1])
print("Total Start_End Quadrant 3 to 3: ",dictFirst_End['3'][2])
print("Total Start_End Quadrant 3 to 4: ",dictFirst_End['3'][3])
print("Total Start_End Quadrant 4 to 1: ",dictFirst_End['4'][0])
print("Total Start_End Quadrant 4 to 2: ",dictFirst_End['4'][1])
print("Total Start_End Quadrant 4 to 3: ",dictFirst_End['4'][2])
print("Total Start_End Quadrant 4 to 4: ",dictFirst_End['4'][3])
print("Total Percent of Start_End Quadrant 1 to 1: ",dictFirst_End['1'][0]/totalFnEn[0])
print("Total Percent of Start_End Quadrant 1 to 2: ",dictFirst_End['1'][1]/totalFnEn[0])
print("Total Percent of Start_End Quadrant 1 to 3: ",dictFirst_End['1'][2]/totalFnEn[0])
print("Total Percent of Start_End Quadrant 1 to 4: ",dictFirst_End['1'][3]/totalFnEn[0])
print("Total Percent of Start_End Quadrant 2 to 1: ",dictFirst_End['2'][0]/totalFnEn[1])
print("Total Percent of Start_End Quadrant 2 to 2: ",dictFirst_End['2'][1]/totalFnEn[1])
print("Total Percent of Start_End Quadrant 2 to 3: ",dictFirst_End['2'][2]/totalFnEn[1])
print("Total Percent of Start_End Quadrant 2 to 4: ",dictFirst_End['2'][3]/totalFnEn[1])
print("Total Percent of Start_End Quadrant 3 to 1: ",dictFirst_End['3'][0]/totalFnEn[2])
print("Total Percent of Start_End Quadrant 3 to 2: ",dictFirst_End['3'][1]/totalFnEn[2])
print("Total Percent of Start_End Quadrant 3 to 3: ",dictFirst_End['3'][2]/totalFnEn[2])
print("Total Percent of Start_End Quadrant 3 to 4: ",dictFirst_End['3'][3]/totalFnEn[2])
print("Total Percent of Start_End Quadrant 4 to 1: ",dictFirst_End['4'][0]/totalFnEn[3])
print("Total Percent of Start_End Quadrant 4 to 2: ",dictFirst_End['4'][1]/totalFnEn[3])
print("Total Percent of Start_End Quadrant 4 to 3: ",dictFirst_End['4'][2]/totalFnEn[3])
print("Total Percent of Start_End Quadrant 4 to 4: ",dictFirst_End['4'][3]/totalFnEn[3])

#Avg use all around
print("Avg use of quadrant 1: ", dicTotal_Usag['1']/int(len(pattList)))
print("Avg use of quadrant 2: ", dicTotal_Usag['2']/int(len(pattList)))
print("Avg use of quadrant 3: ", dicTotal_Usag['3']/int(len(pattList)))
print("Avg use of quadrant 4: ", dicTotal_Usag['4']/int(len(pattList)))
