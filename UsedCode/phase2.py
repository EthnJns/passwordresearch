'''
This phase was dumped, because the overall pattern searching made no sense
Since the password creation would come solely based from statistics
This phase was not done on other data sets, it was also a terribly
Inneficient way of analyzing for patterns
'''

import re
import numpy
import scipy
import pandas
import tensorflow as tf
'''
Phase 2: Pattern Searching
Patterns we are searching or is the # of times qudrabts shift from one to the other
We will get a record of all patterns
Condense it and remove repeating patterns
Find the most and least used patterns
'''
Quadrants = {'`':1, '~':1, 'q': 1, 'a': 1, 'z': 1, 'w':1, 's': 1, 'x': 1, 'e' : 1, 'd': 1,'c': 1, 'r': 1, 'f': 1, 'v':1, 't':1, 'g':1, 'b':1, '1':1, '2':1, '3':1, '4':1, '5':1, \
'!':1, '@':1, '#':1, '$':1, '%':1, 'Q':1,'A':1, 'Z':1, 'W':1, 'S':1, 'X':1, 'E':1, 'D':1, 'C':1, 'R':1, 'F':1, 'V':1, 'T':1, 'G':1, 'B':1, 'y':2,'h':2, 'n':2, 'u':2, 'j':2, 'm':2,\
'i':2, 'k':2, ',':2, 'o':2, 'l':2, '.':2, 'p':2, ';':2, '/':2, '[':2, ']':2, '\\':2, '\'':2, '6':2, '7':2, '8':2, '9':2, '0':2, '-':2, '=':2, '^':2, '&':2, '*':2, '(':2, ')':2,\
'_':2, '+':2, 'Y':2, 'H':2, 'N':2, 'U':2, 'J':2, 'M':2, 'I':2, 'K':2, '<':2, 'O':2, 'L':2, '>':2, 'P':2, ':':2, '?':2, '{':2, '"':2, '}':2, '|':2, ' ':2}

#Open the file of passwords and read it into a list
#Then create an empty list
passwords = open("rt.txt", "r", encoding="latin1")
testwords = []
pwdList = passwords.readlines()
patterns = []

#Test to use regular expression matching to try and condense password patterns
#for x in range(20):
 #   testwords.append(pwdList[x])

#Fill the pattern list
print("Filling the pattern list")
for x in pwdList:
    pttrn = ""
    for i in x:
        if i != '\n':
            if Quadrants[i] == 1:
                pttrn = pttrn + "1"
            else:
                pttrn = pttrn + "2"
    patterns.append(pttrn)
switch = 0
same   = 0
total_char_count = 0

print("Getting total number of switches")
for x in patterns:
    for i in range(len(x)-1):
        if(x[i] == x[i+1]):
            same = same + 1
        else:
            switch = switch + 1
        total_char_count = total_char_count + 1
    total_char_count = total_char_count - 1 #We subtract 1 because we are counting the edges not the vertices

avg_switch_per_word = int(switch / int(len(patterns)))
avg_same_per_word = int(same / int(len(patterns)))






# This will be used for truncating patterns

patterns.sort()
pattern_dict = {}

print("Truncating patterns of different quadrants")
#Truncate the pattern list
for i in range(len(patterns)-1):
    cycle = 0
    counter = 0
    if patterns[i] == patterns[i+1]:
        continue
    if (int(len(patterns[i+1])) % int(len(patterns[i]))) == 0:
        cycle = int(len(patterns[i+1])) / int(len(patterns[i]))
        cycle = int(cycle)
        for x in range(cycle):
            if patterns[i]  == patterns[i + 1][x*int(len(patterns[x])):(x*int(len(patterns[i]))) + len(patterns[i])]:
                counter = counter + 1
        if counter == cycle:
            patterns[i+1] = patterns[i]
    else:
        cycle = int(len(patterns[i+1])) / int(len(patterns[i]))
        cycle = int(cycle)
        sub_pat_len = int(len(patterns[i+1])) % int(len(patterns[i]))
        for x in range(cycle):
            if patterns[i]  == patterns[i + 1][x*int(len(patterns[i])):int(len(patterns[i]))]:
                counter = counter + 1
            if counter == cycle:
                if patterns[i][0:sub_pat_len] == patterns[i+1][cycle*int(len(patterns[x])):]:
                    patterns[i + 1] = patterns[i]


print("Commmening final truncation for patterns of all one quadrant")
#Now truncate all that which are all of one quadrant
for x in range(len(patterns)):
    val = patterns[x][0]
    is_all = True
    for i in patterns[x]:
        if i != val:
            is_all = False
    if is_all:
        patterns[x] = str(val+val+val+val)
#Add to pattern dictionary

"Adding patterns into dictionary to count instances"
for x in patterns:
    if x not in pattern_dict.keys():
        pattern_dict[x] = 1
    else:
        pattern_dict[x] = pattern_dict[x] + 1

print("Creating two lists to count in order")
pattern_name = []
pattern_inst = []

print("Entering while loop")
#This will actually take forever N^2
while int(len(pattern_dict)) > 0:
    lrgPatt = ""
    largest = 0
    for x in pattern_dict:
        if pattern_dict[x] > largest:
            largest = pattern_dict[x]
            lrgPatt = x
    pattern_name.append(lrgPatt)
    pattern_inst.append(largest)
    del pattern_dict[lrgPatt]

print("Opening and writing to file")
pattern_file = open("patterns.txt", "w+")
for x in range(len(pattern_name)):
    pattern_file.write(pattern_name[x] + " \t\t" + str(pattern_inst[x]) +"\n")

print("Avg switch per words: ", avg_switch_per_word)
print("Avg same per words: ", avg_same_per_word)
