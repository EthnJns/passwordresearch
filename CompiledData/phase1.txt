Percantage of passwords starting in Quadrant 1              :  59.405274640152015
Percantage of passwords starting in Quadrant 2              :  40.569551138874694
Percantage of passwords ending in Quadrant 1                :  56.888497508992764
Percantage of passwords ending in Quadrant 2                :  43.07899537440122
Percantage of passwords starting and ending in same quadrant:  34.20142630365944
Percantage of passwords starting and ending in diff quadrant:  65.76606657973454
Percentage of Q1 in first half of passwords                 :  51.587808118209736
Percentage of Q2 in first half of passwords                 :  33.343658269481
Percentage of Q1 in second half of passwords                :  43.21592538965611
Percentage of Q2 in second half of passwords                :  34.22176122432162
Percentage of Q1 & Q2 in first half of passwords            :  15.068533612309274
Percentage of Q1 & Q2 in second half of passwords           :  22.562313386022275
Total # of end_Q1   :  13848004
Total # of end_Q2   :  10486445
Total # of start_Q1 :  14460647
Total # of start_Q2 :  9875587
Total # of same     :  8325435
Total # of differ   :  16009014
Total # frst_half_Q1:  12557691
Total # frst_half_Q2:  8116634
Total # scnd_half_Q1:  10519777
Total # scnd_half_Q2:  8330385
Total # same_half_1 :  3668037
Total # same_half_2 :  5492200
Total # of passwords:  24342362
