patternsFiles4 = ["passwords_4_patt.txt", "ry_4_patt.txt", "phpbb_4_patt.txt"]
patternsFiles8 = ["passwords_8_patt.txt", "ry_8_patt.txt", "phpbb_8_patt.txt"]

#Time to collect all the data
#Dictionaries for 4 quadrants
in_first_section = {'1': 0, '2':0, '3':0, '4':0,'5': 0, '6':0, '7':0, '8':0}

in_scond_section = {'1': 0, '2':0, '3':0, '4':0,'5': 0, '6':0, '7':0, '8':0}

in_third_section = {'1': 0, '2':0, '3':0, '4':0,'5': 0, '6':0, '7':0, '8':0}

in_fourt_section = {'1': 0, '2':0, '3':0, '4':0,'5': 0, '6':0, '7':0, '8':0}

#Lists of sections
section_1 = []
section_2 = []
section_3 = []
section_4 = []

#The number of sections will be how far after each character we take the substring
#Populate the lists of sections 1 - 4 to check for instances
for file_name in patternsFiles8:
	tempR = open(file_name, 'r')	
	pattList = tempR.readlines()	
	for pattern in pattList:
		size_of_pattern = int(len(pattern))
		if(size_of_pattern < 4):
			continue
		number_of_sections = int(size_of_pattern / 4)
		if(size_of_pattern % 4 >= 2 ):
			if size_of_pattern == 6:
				number_of_sections = 1
			else:
				number_of_sections = number_of_sections = 1
		section_1.append(pattern[0: number_of_sections])
		section_2.append(pattern[number_of_sections: number_of_sections + number_of_sections])
		section_3.append(pattern[number_of_sections + number_of_sections: number_of_sections + number_of_sections + number_of_sections])
		section_4.append(pattern[number_of_sections + number_of_sections + number_of_sections:])


	for part in section_1:
		for character in part:
			in_first_section[character] =  in_first_section[character] + 1

	for part in section_2:
		for character in part:
			in_scond_section[character] =  in_scond_section[character] + 1

	for part in section_3:
		for character in part:
			in_third_section[character] =  in_third_section[character] + 1

	for part in section_4:
		for character in part:
			if character == '\n':
				continue
			in_fourt_section[character] =  in_fourt_section[character] + 1

	
	total1 = in_first_section['1'] + in_scond_section['1'] + in_third_section['1'] + in_fourt_section['1'] 

	total2 = in_first_section['2'] + in_scond_section['2'] + in_third_section['2'] + in_fourt_section['2'] 

	total3 = in_first_section['3'] + in_scond_section['3'] + in_third_section['3'] + in_fourt_section['3']

	total4 = in_fourt_section['4'] + in_scond_section['4'] + in_third_section['4'] + in_fourt_section['4']

	total5 = in_first_section['5'] + in_scond_section['5'] + in_third_section['5'] + in_fourt_section['5']

	total6 = in_first_section['6'] + in_scond_section['6'] + in_third_section['6'] + in_fourt_section['6']

	total7 = in_first_section['7'] + in_scond_section['7'] + in_third_section['7'] + in_fourt_section['7']

	total8 = in_first_section['8'] + in_scond_section['8'] + in_third_section['8'] + in_fourt_section['8']
	print("--------------------"+file_name+"------------------------")
	print("Total amount of quadrant 1 in part 1: ", in_first_section['1'])
	print("Total amount of quadrant 2 in part 1: ", in_first_section['2'])
	print("Total amount of quadrant 3 in part 1: ", in_first_section['3'])
	print("Total amount of quadrant 4 in part 1: ", in_first_section['4'])
	print("Total amount of quadrant 5 in part 1: ", in_first_section['5'])
	print("Total amount of quadrant 6 in part 1: ", in_first_section['6'])
	print("Total amount of quadrant 7 in part 1: ", in_first_section['7'])
	print("Total amount of quadrant 8 in part 1: ", in_first_section['8'])

	print("Total amount of quadrant 1 in part 2: ", in_scond_section['1'])
	print("Total amount of quadrant 2 in part 2: ", in_scond_section['2'])
	print("Total amount of quadrant 3 in part 2: ", in_scond_section['3'])
	print("Total amount of quadrant 4 in part 2: ", in_scond_section['4'])
	print("Total amount of quadrant 5 in part 2: ", in_scond_section['5'])
	print("Total amount of quadrant 6 in part 2: ", in_scond_section['6'])
	print("Total amount of quadrant 7 in part 2: ", in_scond_section['7'])
	print("Total amount of quadrant 8 in part 2: ", in_scond_section['8'])

	print("Total amount of quadrant 1 in part 3: ", in_third_section['1'])
	print("Total amount of quadrant 2 in part 3: ", in_third_section['2'])
	print("Total amount of quadrant 3 in part 3: ", in_third_section['3'])
	print("Total amount of quadrant 4 in part 3: ", in_third_section['4'])
	print("Total amount of quadrant 5 in part 3: ", in_third_section['5'])
	print("Total amount of quadrant 6 in part 3: ", in_third_section['6'])
	print("Total amount of quadrant 7 in part 3: ", in_third_section['7'])
	print("Total amount of quadrant 8 in part 3: ", in_third_section['8'])

	print("Total amount of quadrant 1 in part 4: ", in_fourt_section['1'])
	print("Total amount of quadrant 2 in part 4: ", in_fourt_section['2'])
	print("Total amount of quadrant 3 in part 4: ", in_fourt_section['3'])
	print("Total amount of quadrant 4 in part 4: ", in_fourt_section['4'])
	print("Total amount of quadrant 5 in part 4: ", in_fourt_section['5'])
	print("Total amount of quadrant 6 in part 4: ", in_fourt_section['6'])
	print("Total amount of quadrant 7 in part 4: ", in_fourt_section['7'])
	print("Total amount of quadrant 8 in part 4: ", in_fourt_section['8'])

	print("Total % amount of quadrant 1 in part 1: ", in_first_section['1']/total1)
	print("Total % amount of quadrant 2 in part 1: ", in_first_section['2']/total2)
	print("Total % amount of quadrant 3 in part 1: ", in_first_section['3']/total3)
	print("Total % amount of quadrant 4 in part 1: ", in_first_section['4']/total4)
	print("Total % amount of quadrant 5 in part 1: ", in_first_section['5']/total5)
	print("Total % amount of quadrant 6 in part 1: ", in_first_section['6']/total6)
	print("Total % amount of quadrant 7 in part 1: ", in_first_section['7']/total7)
	print("Total % amount of quadrant 8 in part 1: ", in_first_section['8']/total8)

	print("Total % amount of quadrant 1 in part 2: ", in_scond_section['1']/total1)
	print("Total % amount of quadrant 2 in part 2: ", in_scond_section['2']/total2)
	print("Total % amount of quadrant 3 in part 2: ", in_scond_section['3']/total3)
	print("Total % amount of quadrant 4 in part 2: ", in_scond_section['4']/total4)
	print("Total % amount of quadrant 5 in part 2: ", in_scond_section['5']/total5)
	print("Total % amount of quadrant 6 in part 2: ", in_scond_section['6']/total6)
	print("Total % amount of quadrant 7 in part 2: ", in_scond_section['7']/total7)
	print("Total % amount of quadrant 8 in part 2: ", in_scond_section['8']/total8)

	print("Total % amount of quadrant 1 in part 3: ", in_third_section['1']/total1)
	print("Total % amount of quadrant 2 in part 3: ", in_third_section['2']/total2)
	print("Total % amount of quadrant 3 in part 3: ", in_third_section['3']/total3)
	print("Total % amount of quadrant 4 in part 3: ", in_third_section['4']/total4)
	print("Total % amount of quadrant 5 in part 3: ", in_third_section['5']/total5)
	print("Total % amount of quadrant 6 in part 3: ", in_third_section['6']/total6)
	print("Total % amount of quadrant 7 in part 3: ", in_third_section['7']/total7)
	print("Total % amount of quadrant 8 in part 3: ", in_third_section['8']/total8)

	print("Total % amount of quadrant 1 in part 4: ", in_fourt_section['1']/total1)
	print("Total % amount of quadrant 2 in part 4: ", in_fourt_section['2']/total2)
	print("Total % amount of quadrant 3 in part 4: ", in_fourt_section['3']/total3)
	print("Total % amount of quadrant 4 in part 4: ", in_fourt_section['4']/total4)
	print("Total % amount of quadrant 5 in part 4: ", in_fourt_section['5']/total5)
	print("Total % amount of quadrant 6 in part 4: ", in_fourt_section['6']/total6)
	print("Total % amount of quadrant 7 in part 4: ", in_fourt_section['7']/total7)
	print("Total % amount of quadrant 8 in part 4: ", in_fourt_section['8']/total8)
	print("")
	tempR.close()
